package exam;

public class Recursion 
{
	public static void main(String[] ags)
	{
		//test without recursion
		int[] arr1 = {2, 3, 5, 6, 10, 12};
		int res1 = nonRecursiveCount(arr1, 0);
		System.out.println(res1);
		
		//test with recursion
		int[] arr2 = {2, 3, 5, 6, 10, 12};
		int res2 = recursiveCount(arr2, 0);
		System.out.println(res2);
		
	}
	
	public static int recursiveCount(int[] numbers, int n)
	{
		int count;
		if(n < numbers.length)
		{
			count = recursiveCount(numbers, n + 1);
			if(numbers[n] < 10 && n % 2 == 0 && numbers[n] >= n)
			{
				return count + 1;
			}
			return count;
			
		}
		
		else
		{
			return 0;
		}
	}
	
	
	//picturing it without recursion
	public static int nonRecursiveCount(int[] numbers, int n)
	{
		int index = 0;
		int count = 0;
		for(int num : numbers)
		{
			if(num < 10 && index % 2 == 0 && index >= n)
			{
				count++;
			}
			index++;
		}
		return count;
	}
}
