package exam;
import java.util.*;

public class DieRollGame 
{
	private int userMoney;
	private Random rand = new Random();
	
	public DieRollGame()
	{
		this.userMoney = 250;
	}
	
	public String playRound(String choice, String sAmount)
	{
		try
		{
			//parse amount String into int
			int amount = Integer.parseInt(sAmount);
			
			//message that's going to go to the user
			String msg = "";
			
			//die rolled
			int cpu = rand.nextInt(6) + 1;
			
			if(amount <= getUserMoney())
			{
			
				if(choice.equals("even") && cpu % 2 == 0)
				{
					//win
					msg = "Rolled " + cpu + ", you won " + amount + "!";
					this.userMoney += amount;
				}
				
				else if(choice.equals("even") && cpu % 2 != 0)
				{
					//loss
					msg = "Rolled " + cpu + ", you lost " + amount + ".";
					this.userMoney -= amount;
				}
				
				else if(choice.equals("odd") && cpu % 2 != 0)
				{
					//win
					msg = "Rolled " + cpu + ", you won " + amount + "!";
					this.userMoney += amount;
				}
				
				else if(choice.equals("odd") && cpu % 2 == 0)
				{
					//loss
					msg = "Rolled " + cpu + ", you lost " + amount + ".";
					this.userMoney -= amount;
				}
				
				return msg;
			}
				
			return "Amount bet exceeds what you have!";
		}
		
		catch(NumberFormatException e)
		{
			return "Amount entered invalid.";
		}
	}
	
	public int getUserMoney()
	{
		return this.userMoney;
	}
}
