package exam;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class DieRollEventListener implements EventHandler<ActionEvent>
{
	private String amount;
	private String choice;
	private TextField moneyField;
	private TextField statusField;
	private DieRollGame game;
	
	public DieRollEventListener(String vAmount, String vChoice, TextField vMoney, TextField vStatus, DieRollGame vGame)
	{
		this.amount = vAmount;
		this.choice = vChoice;
		this.moneyField = vMoney;
		this.statusField = vStatus;
		this.game = vGame;
	}
	
	@Override
	public void handle(ActionEvent e) 
	{
		this.statusField.setText("Status: " + this.game.playRound(this.choice, this.amount));
		this.moneyField.setText("Money: $" + this.game.getUserMoney());
	}
}
