package exam;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class DieRollApplication extends Application
{
	private DieRollGame game = new DieRollGame();
	
	@Override
	public void start(Stage stage)
	{
		//container and layout
		Group root = new Group();
		
		//create components
		VBox overall = new VBox();
		HBox welcome = new HBox();
		HBox placeBet = new HBox();
		HBox betBtns = new HBox();
		HBox money = new HBox();
		HBox message = new HBox();
		TextField welc = new TextField("Welcome! Enter your bet below and then click on what you're betting:");
		TextField bet = new TextField();
		TextField mny = new TextField("Money: $250");
		TextField msg = new TextField("Status: ");
		Button even = new Button("even");
		Button odd = new Button("odd");
		
		//button event listeners
		DieRollEventListener actionEven = new DieRollEventListener(bet.getText(), even.getText(), mny, msg, game);
		even.setOnAction(actionEven);
		DieRollEventListener actionOdd = new DieRollEventListener(bet.getText(), odd.getText(), mny, msg, game);
		odd.setOnAction(actionOdd);
		
		//extra properties
		welc.setPrefWidth(420);
		msg.setPrefWidth(350);
		welc.setEditable(false);
		mny.setEditable(false);
		msg.setEditable(false);
		
		//build interface
		welcome.getChildren().add(welc);
		placeBet.getChildren().add(bet);
		betBtns.getChildren().addAll(even, odd);
		money.getChildren().add(mny);
		message.getChildren().add(msg);
		overall.getChildren().addAll(welcome, placeBet, betBtns, money, message);
		root.getChildren().add(overall);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 800, 600);
		scene.setFill(Color.BLACK);
		
		//associate scene to stage and show
		stage.setTitle("Die Roll App");
		stage.setScene(scene);
		stage.show();
	}
	
	public static void main(String[] args)
	{
		Application.launch(args);
	}
}
